import locale

from django import template
from django.urls import reverse

register = template.Library()


@register.simple_tag
def is_active(current_path, link_path, param=None):
    try:
        if param is None:
            if reverse(link_path) == current_path:
                return 'is-active'
        else:
            if reverse(link_path, param) == current_path:
                return 'is-active'
            else:
                return ''
    except:
        return ''


@register.filter
def money_format(price):
    try:
        locale.setlocale(locale.LC_ALL, 'en_NG')
        return locale.currency(price, grouping=True)
    except Exception as e:
        return price
