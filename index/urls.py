from django.conf.urls import url
from django.contrib.auth.views import (
    login,
    logout,
)

from index.views import (
    login_,
    register,
    profile,
    lockscreen,
    error_page,
    create_admin,
    subscriptions,
    admin_dashboard,
    subscriptions_list
)

urlpatterns = [
    url(r'^login/$', login_, name='login'),
    url(r'^logout/$', logout, name='logout'),
    url(r'^lock/$', lockscreen, name='lock_screen'),
    url(r'^register/$', register, name='register'),

    url(r'^profile/$', profile, name='profile'),
    url(r'^error/$', error_page, name='error_page'),

    url(r'^administrator/$', admin_dashboard, name='admin_dashboard'),
    url(r'^subscriptions/list/$', subscriptions_list, name='subscriptions_list'),
    url(r'^administrator/create/$', create_admin, name='create_admin'),
    url(r'^subscription/(?P<key>\d+)/$', subscriptions, name='subscriptions'),
]
