from django import forms
from index.models import (
    Institution,
    Subscription,
)

class UserLoginForm(forms.Form):
    username = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'placeholder': 'haroun@gmail.com'}))
    password = forms.CharField(required=True, widget=forms.PasswordInput(
        attrs={'placeholder': '*******'}))


class AddInstitutionFrom(forms.ModelForm):

    class Meta:
        model = Institution
        fields = (
            'name',
            'address',
            'contact',
        )


class AddSubscriptionFrom(forms.ModelForm):

    class Meta:
        model = Subscription
        fields = (
            'amount_paid',
            'validity',
        )