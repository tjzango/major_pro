# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Institution(models.Model):
    name = models.CharField(max_length=200)
    address = models.TextField()
    contact = models.CharField(max_length=200)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Subscription(models.Model):
    institution = models.ForeignKey(Institution, related_name='institute')
    amount_paid = models.IntegerField(default=0)
    validity = models.DateField()
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} valid to {}'.format(self.institution.name, self.validity)


"""
import datetime
date = datetime.date.today()
start_week = date - datetime.timedelta(date.weekday())
end_week = start_week + datetime.timedelta(7)
entries = Entry.objects.filter(created_at__range=[start_week, end_week])
"""
