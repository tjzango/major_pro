from io import BytesIO

from django.shortcuts import (
    HttpResponse,
)
from django.template.loader import get_template
from xhtml2pdf import pisa

from django.contrib.auth.models import User
from staff.models import Account
from django.shortcuts import redirect


def user(request):
    try:
        users = User.objects.get(username=request.user, is_staff=True)
        if users:
            return {
                'administrator': users,
            }
    except User.DoesNotExist:
        try:
            users = Account.objects.get(user__username=request.user)
            if users:
                return {
                    'account': users,
                }
        except Account.DoesNotExist:
            return {'account': 0}




def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return None
