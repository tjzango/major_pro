# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import messages
from django.contrib.auth import (
    authenticate,
    login,
)
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.shortcuts import (
    render,
    redirect,
)

from index.forms import (
    UserLoginForm,
    AddInstitutionFrom,
    AddSubscriptionFrom,
)
from index.models import (
    Institution,
    Subscription,
)
from staff.models import Account


# Create your views here.
def register(request):
    context = {

    }
    return render(request, 'registration/register.html', context)


def login_(request):
    next = request.GET.get('next')
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get('password')
        try:
            user = User.objects.get(username=username, is_staff=True)
            if user.is_active:
                user = authenticate(username=username, password=password)
                if user:
                    login(request, user)
                    if next:
                        return redirect(next)
                    return redirect("index:admin_dashboard")
                messages.success(request, 'Incorrect username or password')
                return redirect("/login")
            messages.success(request, 'Authorisation Access Suspended')
        except User.DoesNotExist:
            try:
                user = Account.objects.get(user__username=username)
                if user.user.is_active:
                    subscription = Subscription.objects.filter(institution=user.employee.institution)
                    print(1)
                    if subscription:
                        print(2)
                        import datetime
                        subscription = subscription.last()
                        if subscription.validity >= datetime.date.today().today():
                            print(3)
                            user = authenticate(username=username, password=password)
                            if user:
                                login(request, user)
                                if next:
                                    return redirect(next)
                                return redirect("index:profile")
                        else:
                            print(4)
                            messages.error(request, 'Your subscription has expired')
                            return redirect("/login")
                    else:
                        print(5)
                        user = authenticate(username=username, password=password)
                        if user:
                            login(request, user)
                            if next:
                                return redirect(next)
                            messages.warning(request, 'Your hove no subscription kindly make your sub payment')
                            return redirect("index:profile")
                messages.success(request, 'Username password does not match')
                print(8)
            except Account.DoesNotExist:
                messages.success(request, 'Username password does not match')
    return render(request, "registration/login.html", {"form": form})


def create_admin(request):
    form = UserCreationForm(request.POST or None)
    if form.is_valid():
        user = form.save()
        user.is_staff = True
        user.save()
        messages.success(request, 'Administrator Created !!!')
        return redirect('index:create_admin')
    context = {
        'form': form,
        'administrators': User.objects.filter(is_staff=True),
    }
    return render(request, 'admin/create_admin.html', context)


def lockscreen(request):
    account = Account.objects.get(user=request.user)
    account.lock = True
    account.save()
    return render(request, 'registration/lockscreen.html')


def admin_dashboard(request):
    form = AddInstitutionFrom(request.POST or None)
    if form.is_valid():
        form.save()
        messages.success(request, 'Institution Created')
        return redirect('index:admin_dashboard')
    context = {
        'form': form,
        'institutions': Institution.objects.all(),
    }
    return render(request, 'admin/dashboard.html', context)


def admin_user(request):
    form = AddInstitutionFrom(request.POST or None)
    if form.is_valid():
        form.save()
        messages.success(request, 'Institution Created')
        return redirect('index:admin_dashboard')
    context = {
        'form': form,
        'institutions': Institution.objects.all(),
    }
    return render(request, 'admin/dashboard.html', context)


def subscriptions(request, key):
    form = AddSubscriptionFrom(request.POST or None)
    if form.is_valid():
        account = Account.objects.get(user=request.user)
        subscription = form.save(commit=False)
        subscription.institution = account.employee.institution
        subscription.save()
        messages.success(request, 'Subscription valid till {}'.format(subscription.validity))
        return redirect('index:subscriptions', key)

    context = {
        'form': form,
        'institution': Institution.objects.get(id=key),
        'subscriptions': Subscription.objects.filter(institution_id=key),
    }
    return render(request, 'admin/subscriptions.html', context)


def subscriptions_list(request):
    context = {
        'subscriptions': Subscription.objects.all()
    }
    return render(request, 'admin/subscriptions_list.html', context)


def profile(request):
    return render(request, 'user/profile.html')


def error_page(request):
    context = {
        'title_head': '404',
        'title': 'Page Not Fount',
        'content': "Sorry, we couldn't find the page you are looking for..."
    }
    return render(request, 'error.html', context)
