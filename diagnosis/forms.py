from django import forms

from diagnosis.models import (
    Scanning,
    Order,
    RequestScanning,
)


class AddScanningForm(forms.ModelForm):
    class Meta:
        model = Scanning
        fields = (
            'name',
            'price'
        )


class OrderRequestForm(forms.ModelForm):

    class Meta:
        model = Order
        fields = [
            'customer',
            'phone',
            'paid',
            'cash',
            'transfer',
            'request_by',
        ]


class ReportForm(forms.ModelForm):
    class Meta:
        model = RequestScanning
        fields = [
            'report',
            'imaging_no',
        ]


class AddForm(forms.Form):
    update = forms.BooleanField(
        required=False,
        initial=False,
        widget=forms.HiddenInput
    )
