# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from diagnosis import models
# Register your models here.

admin.site.register(models.Scanning)
admin.site.register(models.RequestScanning)
admin.site.register(models.Order)
