# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from cart.cart import Cart
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import (
    render,
    reverse,
    redirect,
    HttpResponse,
    get_object_or_404,
    HttpResponseRedirect,
)
from django.template.loader import get_template

from diagnosis.forms import (
    AddForm,
    Scanning,
    ReportForm,
    Order,
    RequestScanning,
    AddScanningForm,
    OrderRequestForm,
)
from diagnosis.utility import (
    render_to_pdf,
    get_rank,
)
from staff.models import Account


# Create your views here.
def scanning(request):
    rank = get_rank(request)
    if rank[0] == 'Consultant':
        return HttpResponseRedirect(reverse('index:error_page'))
    form = AddScanningForm(request.POST or None)

    if form.is_valid():
        account = Account.objects.get(user=request.user)
        scan = form.save(commit=False)
        scan.account = account
        scan.save()
        messages.success(request, 'Added a {}'.format(scan.name))
        return HttpResponseRedirect(reverse('diagnosis:add'))

    context = {
        "form": form,
        'scannings': Scanning.objects.filter(account__employee__institution=rank[1])
    }
    return render(request, "admin/add_scan.html", context)


@login_required
def request_scanning(request):
    rank = get_rank(request)
    if rank[0] == 'Consultant':
        return HttpResponseRedirect(reverse('index:error_page'))
    form = OrderRequestForm(None or request.POST)
    if form.is_valid():
        cart = Cart(request)
        order = form.save(commit=False)
        order.total = int(form.cleaned_data.get('transfer')) + int(form.cleaned_data.get('cash'))
        order.attained_by = Account.objects.get(user=request.user)
        order.save()

        for item in cart:
            RequestScanning.objects.create(order=order, scanning=item.product,
                                           institution=order.attained_by.employee.institution)

        cart.clear()
        return redirect('diagnosis:invoice', order.id)

    context = {
        'form': form,
        'cart': Cart(request),
        'scannings': Scanning.objects.filter(account__employee__institution=rank[1])
    }
    return render(request, 'admin/request_scanning.html', context)


@login_required
def invoice(request, key):
    order = Order.objects.get(id=key)
    due = order.get_total_cost() - order.total
    context = {
        'order': order,
        'due': due,
        'basket': RequestScanning.objects.filter(order=order),
    }
    return render(request, 'invoice.html', context)


@login_required
def add_item_to_cart(request, key):
    rank = get_rank(request)
    if rank[0] == 'Consultant':
        return HttpResponseRedirect(reverse('index:error_page'))
    item = Scanning.objects.get(id=key)
    cart = Cart(request)
    try:
        cart.update(product=item, quantity=1, unit_price=item.price)
    except Exception:
        cart.add(item, item.price, quantity=1)
    return redirect('/diagnosis/request/scanning')


@login_required
def remove_item_from_cart(request, key):
    rank = get_rank(request)
    if rank == 'Consultant':
        return HttpResponseRedirect(reverse('index:error_page'))
    product = Scanning.objects.get(id=key)
    cart = Cart(request)
    cart.remove(product)
    return redirect('/diagnosis/request/scanning')


@login_required
def clear_cart(request):
    rank = get_rank(request)
    if rank == 'Consultant':
        return HttpResponseRedirect(reverse('index:error_page'))
    cart = Cart(request)
    cart.clear()
    return redirect('/diagnosis/request/scanning')


@login_required
def report(request):
    rank = get_rank(request)
    if rank[0] == 'Receptionist':
        return HttpResponseRedirect(reverse('index:error_page'))
    context = {
        'scannings': RequestScanning.objects.filter(institution=rank[1])
    }
    return render(request, 'admin/compute.html', context)


@login_required
def status(request):
    rank = get_rank(request)
    if rank[0] == 'Consultant':
        return HttpResponseRedirect(reverse('index:error_page'))
    context = {
        'scannings': RequestScanning.objects.filter(institution=rank[1]),
    }
    return render(request, 'admin/status.html', context)


@login_required
def report_write(request, key):
    rank = get_rank(request)
    if rank[0] == 'Receptionist':
        return HttpResponseRedirect(reverse('index:error_page'))
    instance = get_object_or_404(RequestScanning, id=key)
    if request.method == 'POST':
        form = ReportForm(request.POST)
        if form.is_valid():
            instance.report = form.cleaned_data.get('report')
            instance.done = True
            instance.imaging_no = form.cleaned_data.get('imaging_no')
            instance.save()
            messages.success(request, '{} report for {}'.format(instance.scanning.name, instance.order.customer))
        return HttpResponseRedirect(reverse('diagnosis:report'))

    context = {
        'form': ReportForm(),
        'instance': instance,
    }
    return render(request, 'write_report.html', context)


def printing(request, key):
    rank = get_rank(request)
    if rank[0] == 'Consultant':
        return HttpResponseRedirect(reverse('index:error_page'))
    scanning = RequestScanning.objects.get(id=key, institution=rank[1])
    context = {

        'basket': scanning
    }
    return render(request, 'print.html', context)

