# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.db import models

from index.models import Institution
from staff.models import Account


# Create your models here.


class Scanning(models.Model):
    name = models.CharField(max_length=100)
    price = models.PositiveIntegerField(default=0)
    account = models.ForeignKey(Account, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('diagnosis:request')


class Order(models.Model):
    customer = models.CharField(max_length=100)
    phone = models.CharField(max_length=20)
    created = models.DateTimeField(auto_now_add=True)
    paid = models.BooleanField(default=True)
    cash = models.IntegerField(default=0)
    transfer = models.IntegerField(default=0)
    total = models.IntegerField(default=0)
    request_by = models.CharField(max_length=100)
    attained_by = models.ForeignKey(Account, on_delete=models.CASCADE)

    def __str__(self):
        return self.customer

    def get_total_cost(self):
        return sum(item.get_cost() for item in self.items.all())


class RequestScanningManager(models.Manager):
    def ours(self):
        qs = super(RequestScanningManager, self).filter(self.order.attained_by.employee.institution)
        return qs


class RequestScanning(models.Model):
    order = models.ForeignKey(Order, related_name='items')
    scanning = models.ForeignKey(Scanning, related_name='order_items')
    done = models.BooleanField(default=False)
    report = models.TextField(null=True, blank=True)
    imaging_no = models.CharField(max_length=100)
    institution = models.ForeignKey(Institution, on_delete=models.CASCADE)
    # objects = RequestScanningManager

    def __str__(self):
        return '{}'.format(self.order)

    def get_customer(self):
        return self.order.customer

    def get_phone(self):
        return self.order.phone

    def get_absolute_url(self):
        return reverse('sale:pdf',
                       args=[self.order.id])

    def get_report_url(self):
        return reverse('diagnosis:report-write',
                       args=[self.id])

    def get_print_url(self):
        return reverse('diagnosis:print_report',
                       args=[self.id])

    def get_cost(self):
        return self.scanning.price
