from django.conf.urls import url

from diagnosis.views import (
    clear_cart,
    scanning,
    request_scanning,
    add_item_to_cart,
    remove_item_from_cart,

    report,
    printing,
    invoice,
    status,
    report_write,
)

urlpatterns = [
    url(r'^$', scanning, name='add'),
    url(r'^print/(?P<key>[0-9]+)/$', printing, name='print'),
    url(r'^invoice/(?P<key>[0-9]+)/$', invoice, name='invoice'),
    url(r'^clear_cart/$', clear_cart, name='clear_cart'),
    url(r'^status/$', status, name='status'),
    url(r'^request/scanning$', request_scanning, name='request_scanning'),
    url(r'^new/cart/(?P<key>[0-9]+)$', add_item_to_cart, name='cart_add'),
    url(r'^new/cart/remove/(?P<key>[0-9]+)/$', remove_item_from_cart, name='cart_remove'),
    url(r'^write/report/(?P<key>[0-9]+)$', report_write, name='report_write'),
    url(r'^report/$', report, name='report'),
]
