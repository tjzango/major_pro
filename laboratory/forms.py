from django import forms

from laboratory.models import (
    Test,
    # Order,
    # RequestScanning,
)


class AddTestForm(forms.ModelForm):
    class Meta:
        model = Test
        fields = (
            'name',
            'price'
        )

'''
class OrderRequestForm(forms.ModelForm):

    class Meta:
        model = Order
        fields = [
            'customer',
            'phone',
            'paid',
            'cash',
            'transfer',
            'request_by',
        ]


class ReportForm(forms.ModelForm):
    class Meta:
        model = RequestScanning
        fields = [
            'report',
            'imaging_no',
        ]


class AddForm(forms.Form):
    update = forms.BooleanField(
        required=False,
        initial=False,
        widget=forms.HiddenInput
    )

'''