from django import forms
from staff.models import (
    Employee,
    Account,
)


class AddEmployeeFrom(forms.ModelForm):

    class Meta:
        model = Employee
        fields = (
            'name',
            'sex',
            'status',
            'date_of_birth',
            'date_employed',
            'passport',
            'nationality',
            'address',
            'phone',
        )


class CreateUserAccountForm(forms.ModelForm):
    password = forms.CharField(required=True, widget=forms.PasswordInput(
        attrs={'placeholder': 'Password'}), label='Password:')
    username = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'placeholder': 'Username'}), label='Username:')

    class Meta:
        model = Account
        fields = (
            'employee',
            'username',
            'category',
            'password',
        )

    def __init__(self, account, *args, **kwargs):
        super(CreateUserAccountForm, self).__init__(*args, **kwargs)
        self.fields['employee'].queryset = Employee.objects.filter(institution=account.employee.institution)
