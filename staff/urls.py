from django.conf.urls import url

from staff.views import (
    employee,
    user_account,
    employee_add,
    employee_check,
)

urlpatterns = [
    url(r'^employee/$', employee, name='employee'),
    url(r'^employee/add/$', employee_add, name='employee_add'),
    url(r'^account/user/$', user_account, name='account'),
    url(r'^employee/(?P<key>\d+)/$', employee_check, name='employee_check'),
]
