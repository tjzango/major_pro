# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

from index.models import Institution

STAFF_CATEGORY = (
    ('Admin', 'Admin'),
    ('Receptionist', 'Receptionist'),
    ('Consultant', 'Consultant'))
STATUE = (('Married', 'Married'), ('Single', 'Single'))
SEX = (('Male', 'Male'), ('Female', 'Female'), ('Other', 'Other'))


# Create your models here.
class Employee(models.Model):
    institution = models.ForeignKey(Institution, related_name='cooperation')
    name = models.CharField(max_length=100)
    passport = models.ImageField(blank=True, upload_to='passport')
    sex = models.CharField(max_length=20, choices=SEX)
    status = models.CharField(max_length=20, choices=STATUE)
    date_of_birth = models.DateField(max_length=10)
    nationality = models.CharField(max_length=100)
    address = models.TextField()
    phone = models.IntegerField()
    date_employed = models.DateField()
    visible = models.BooleanField(default=True)

    def __str__(self):
        return '{}'.format(self.name)


class Account(models.Model):
    user = models.ForeignKey(User, related_name='users')
    employee = models.OneToOneField(Employee, related_name='agent')
    category = models.CharField(max_length=100, choices=STAFF_CATEGORY)
    lock = models.BooleanField(default=False)

    def __str__(self):
        return "{}  {}".format(self.employee.institution, self.user)
