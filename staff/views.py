# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import (
    render,
    redirect,
    HttpResponseRedirect,
)

from staff.forms import (
    AddEmployeeFrom,
    CreateUserAccountForm,
)
from staff.models import (
    Account,
    Employee,
)


# Create your views here.
@login_required
def employee(request):
    instance = Account.objects.get(user=request.user)
    context = {
        'employees': Employee.objects.filter(institution=instance.employee.institution),
    }
    return render(request, 'employee.html', context)


def employee_check(request, key):
    context = {
        'employee_check': Employee.objects.get(id=key),
        'employees': Employee.objects.all(),
    }
    return render(request, 'employee.html', context)


@login_required
def employee_add(request):
    form = AddEmployeeFrom(request.POST or None)
    print ('trying')
    if form.is_valid():
        print ('pass')
        instance = form.save(commit=False)
        account = Account.objects.get(user=request.user)
        instance.institution = account.employee.institution
        instance.save()
        return redirect("/employee")

    print ('Failed')
    context = {
        'form': AddEmployeeFrom,
        'error': form.errors
    }
    return render(request, 'employee_add.html', context)


@login_required
def user_account(request):
    account_instance = Account.objects.get(user=request.user)
    if request.method == 'POST':
        form_post = CreateUserAccountForm(account_instance, request.POST)
        if form_post.is_valid():
            user = User()
            account = form_post.save(commit=False)
            username = form_post.cleaned_data['username']
            password = form_post.cleaned_data['password']
            if username:
                user_name = User.objects.filter(username=username)
                if user_name:
                    messages.error(request, "Username already exist")
                    return redirect('/account/user/')
            if len(password) <= 4:
                messages.error(request, "Passwords must contain at least 5 characters")
                return redirect('/account/user/')

            user.username = username
            user.set_password(password)
            user.save()

            account_instance = Account.objects.get(user=request.user)
            account.institution = account_instance.employee.institution
            account.user = user
            account.save()
            messages.success(request, "Employee account created")
            return redirect("/account/user/")
        else:
            context = {
                'form': form_post,
                'accounts': Account.objects.filter(employee__institution=account_instance.employee.institution),
                'error': form_post.errors
            }
            return render(request, 'users.html', context)
    form = CreateUserAccountForm(account_instance)
    context = {
        'form': form,
        'accounts': Account.objects.filter(employee__institution=account_instance.employee.institution),
    }
    return render(request, 'users.html', context)

