from django.conf.urls import url
from temp.views import (
    index
)


urlpatterns = [
    url(r'^dashboard/$', index, name='dashboard'),
]
